//
//  SwiftCCTests.swift
//  SwiftCCTests
//
//  MIT License
//
//  Copyright (c) 2016 David Skuza
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import XCTest
@testable import SwiftCC

class SwiftCCTests: XCTestCase {
    let ccNumbers: [CreditCard.Issuer: UInt] = [
        CreditCard.Issuer.Visa: 4111111111111111,
        CreditCard.Issuer.MasterCard: 5555555555554444,
        CreditCard.Issuer.AmericanExpress: 378282246310005,
        CreditCard.Issuer.Discover: 6011111111111117,
        CreditCard.Issuer.JCB: 3530111333300000,
        CreditCard.Issuer.DinersClub: 38520000023237,
        CreditCard.Issuer.UnionPay: 6240008631401148,
        CreditCard.Issuer.Maestro: 5000000000000611,
    ]

    func testCardCreation() {
        // Test creating a card with an int
        let cc1 = CreditCard(withNumber: 0)
        XCTAssertEqual(cc1.cardNumber, "0")

        // Test creating a card with a valid string
        let cc2 = CreditCard(withString: "0")
        XCTAssertNotNil(cc2)
        XCTAssertEqual(cc2!.cardNumber, "0")

        // Test creating a card with an invalid string
        let cc3 = CreditCard(withString: "a")
        XCTAssertNil(cc3)
    }

    // https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/credit_card_numbers.htm
    // https://github.com/braintree/card-validator/blob/master/test/unit/card-number.js (Maestro)
    func testCardIssuer() {
        // Visa
        let visa = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.Visa]!)
        XCTAssertNotNil(visa.issuer())
        XCTAssertEqual(visa.issuer(), CreditCard.Issuer.Visa)

        // MasterCard
        let mc = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.MasterCard]!)
        XCTAssertNotNil(mc.issuer())
        XCTAssertEqual(mc.issuer(), CreditCard.Issuer.MasterCard)

        // American Express
        let amex = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.AmericanExpress]!)
        XCTAssertNotNil(amex.issuer())
        XCTAssertEqual(amex.issuer(), CreditCard.Issuer.AmericanExpress)

        // Discover
        let disc = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.Discover]!)
        XCTAssertNotNil(disc.issuer())
        XCTAssertEqual(disc.issuer(), CreditCard.Issuer.Discover)

        // JCB
        let jcb = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.JCB]!)
        XCTAssertNotNil(jcb.issuer())
        XCTAssertEqual(jcb.issuer(), CreditCard.Issuer.JCB)

        // Diners Club
        let dc = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.DinersClub]!)
        XCTAssertNotNil(dc.issuer())
        XCTAssertEqual(dc.issuer(), CreditCard.Issuer.DinersClub)

        // UnionPay
        let up = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.UnionPay]!)
        XCTAssertNotNil(up.issuer())
        XCTAssertEqual(up.issuer(), CreditCard.Issuer.UnionPay)

        // Maestro
        let m = CreditCard(withNumber: self.ccNumbers[CreditCard.Issuer.Maestro]!)
        XCTAssertNotNil(m.issuer())
        XCTAssertEqual(m.issuer(), CreditCard.Issuer.Maestro)

        // Unknown
        let unk = CreditCard(withNumber: 12345678910)
        XCTAssertNil(unk.issuer())
    }

    func validateIssuedCard(_ issuer: CreditCard.Issuer) {
        var card = CreditCard(withNumber: self.ccNumbers[issuer]!)
        XCTAssertTrue(CreditCardValidator.validateNumber(card.cardNumber, against: issuer))
        XCTAssertTrue(CreditCardValidator.validateLuhn(card.cardNumber))

        card.securityCode = issuer == .AmericanExpress ? "1111" : "111"
        XCTAssertTrue(CreditCardValidator.validateSecurityCode(card.securityCode, against: issuer))
        card.securityCode = "11"
        XCTAssertFalse(CreditCardValidator.validateSecurityCode(card.securityCode, against: issuer))

        card.expirationMonth = "1"
        card.expirationYear = "2020"
        XCTAssertTrue(CreditCardValidator.validateExpirationMonth(card.expirationMonth))
        XCTAssertTrue(CreditCardValidator.validateExpirationYear(card.expirationYear))
        XCTAssertTrue(CreditCardValidator.validateExpirationDate(month: card.expirationMonth, year: card.expirationYear))
        card.expirationMonth = "14"
        card.expirationYear = "1942"
        XCTAssertFalse(CreditCardValidator.validateExpirationMonth(card.expirationMonth))
        XCTAssertFalse(CreditCardValidator.validateExpirationYear(card.expirationYear))
        XCTAssertFalse(CreditCardValidator.validateExpirationDate(month: card.expirationMonth, year: card.expirationYear))
    }

    func testValidation() {
        validateIssuedCard(CreditCard.Issuer.Visa)
        validateIssuedCard(CreditCard.Issuer.MasterCard)
        validateIssuedCard(CreditCard.Issuer.AmericanExpress)
        validateIssuedCard(CreditCard.Issuer.Discover)
        validateIssuedCard(CreditCard.Issuer.JCB)
        validateIssuedCard(CreditCard.Issuer.DinersClub)
        validateIssuedCard(CreditCard.Issuer.UnionPay)
        validateIssuedCard(CreditCard.Issuer.Maestro)
    }
}
