//
//  CreditCard.swift
//  SwiftCC
//
//  MIT License
//
//  Copyright (c) 2016 David Skuza
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//


import Foundation

struct CreditCard {
    // Accepted card types are ones accepted by Braintree
    // https://www.braintreepayments.com/faq
    public enum Issuer {
        case Visa
        case MasterCard
        case AmericanExpress
        case Discover
        case JCB
        case DinersClub
        case UnionPay
        case Maestro
    }

    public var cardNumber: String
    public var securityCode: String = ""
    public var expirationMonth: String = ""
    public var expirationYear: String = ""

    // Initialize a card with a number
    init(withNumber: UInt) {
        self.cardNumber = "\(withNumber)"
    }

    // Initialize a card with a string
    // withString must contain only digits 0-9, otherwise this will return nil
    init?(withString: String) {
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: [])
        let extracted = regex.stringByReplacingMatches(in: withString, options: [], range: NSMakeRange(0, withString.characters.count), withTemplate: "")

        if extracted.characters.count == 0 {
            return nil
        }

        self.cardNumber = extracted
    }

    // Return the issuer of a card based on regex matches
    // The regex patterns were found in the following repository
    // https://github.com/braintree/credit-card-type/blob/master/index.js
    public func issuer() -> CreditCard.Issuer? {
        var issuer: CreditCard.Issuer? = nil

        let issuers = [CreditCard.Issuer.Visa: "^4\\d*$",
                       CreditCard.Issuer.MasterCard: "^(5[1-5]|222[1-9]|2[3-6]|27[0-1]|2720)\\d*$",
                       CreditCard.Issuer.AmericanExpress: "^3[47]\\d*$",
                       CreditCard.Issuer.Discover: "^(6011|65|64[4-9])\\d*",
                       CreditCard.Issuer.JCB: "^(2131|1800|35)\\d*$",
                       CreditCard.Issuer.DinersClub: "^3(0[0-5]|[689])\\d*$",
                       CreditCard.Issuer.UnionPay: "^62\\d*$",
                       CreditCard.Issuer.Maestro: "^5[06-9]\\d*$"]

        for (key, value) in issuers {
            let issuerRegex = try! NSRegularExpression(pattern: value, options: [])
            if issuerRegex.matches(in: self.cardNumber, options: [], range: NSMakeRange(0, self.cardNumber.characters.count)).count > 0 {
                issuer = key
                break
            }
        }

        return issuer
    }
}
