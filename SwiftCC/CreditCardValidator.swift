//
//  CreditCardValidator.swift
//  SwiftCC
//
//  MIT License
//
//  Copyright (c) 2016 David Skuza
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import Foundation

class CreditCardValidator {
    // Validate whether a card belongs to a certain issuer via regex
    // The regex patterns were found in the following repository
    // https://github.com/braintree/credit-card-type/blob/master/index.js
    static func validateNumber(_ number: String, against issuer: CreditCard.Issuer) -> Bool {
        // Automatically fail if a string with non-digits exists
        let regex = try! NSRegularExpression(pattern: "[^\\d]", options: [NSRegularExpression.Options.caseInsensitive])
        guard regex.matches(in: number, options: [], range: NSMakeRange(0, number.characters.count)).count == 0 else {
            return false
        }

        var regexString: String
        switch issuer {
        case .Visa:
            regexString = "^4\\d*$"
        case .MasterCard:
            regexString = "^(5[1-5]|222[1-9]|2[3-6]|27[0-1]|2720)\\d*$"
        case .AmericanExpress:
            regexString = "^3[47]\\d*$"
        case .Discover:
            regexString = "^(6011|65|64[4-9])\\d*"
        case .JCB:
            regexString = "^(2131|1800|35)\\d*$"
        case .DinersClub:
            regexString = "^3(0[0-5]|[689])\\d*$"
        case .UnionPay:
            regexString = "^62\\d*$"
        case .Maestro:
            regexString = "^5[06-9]\\d*$"
        }

        let issuerRegex = try! NSRegularExpression(pattern: regexString, options: [])
        return issuerRegex.matches(in: number, options: [], range: NSMakeRange(0, number.characters.count)).count > 0
    }

    // Validate whether a card has a correct security code
    // This is nothing more than a check on the code length
    static func validateSecurityCode(_ code: String, against issuer: CreditCard.Issuer) -> Bool {
        // Automatically fail if a string with non-digits exists
        let regex = try! NSRegularExpression(pattern: "[^\\d]", options: [NSRegularExpression.Options.caseInsensitive])
        guard regex.matches(in: code, options: [], range: NSMakeRange(0, code.characters.count)).count == 0 else {
            return false
        }

        switch issuer {
        case .Visa:
            return code.characters.count == 3
        case .MasterCard:
            return code.characters.count == 3
        case .AmericanExpress:
            return code.characters.count == 4
        case .Discover:
            return code.characters.count == 3
        case .JCB:
            return code.characters.count == 3
        case .DinersClub:
            return code.characters.count == 3
        case .UnionPay:
            return code.characters.count == 3
        case .Maestro:
            return code.characters.count == 3
        }
    }

    // Validate whether a month is within the valid range of [1...12]
    static func validateExpirationMonth(_ month: String) -> Bool {
        // Automatically fail if a string with non-digits exists
        let regex = try! NSRegularExpression(pattern: "[^\\d]", options: [NSRegularExpression.Options.caseInsensitive])
        guard regex.matches(in: month, options: [], range: NSMakeRange(0, month.characters.count)).count == 0 else {
            return false
        }

        let m = Int(month)!
        return m >= 1 && m <= 12
    }

    // Validate whether a year is greater than or equal to the current year
    static func validateExpirationYear(_ year: String) -> Bool {
        // Automatically fail if a string with non-digits exists
        let regex = try! NSRegularExpression(pattern: "[^\\d]", options: [NSRegularExpression.Options.caseInsensitive])
        guard regex.matches(in: year, options: [], range: NSMakeRange(0, year.characters.count)).count == 0 else {
            return false
        }

        let cal = NSCalendar.current
        let curYear = cal.component(Calendar.Component.year, from: Date())
        return Int(year)! >= curYear
    }

    // Validate whether a credit card has expired or not
    // Returns false if the expiration month/year matches the current month/year
    static func validateExpirationDate(month: String, year: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: "[^\\d]", options: [NSRegularExpression.Options.caseInsensitive])
        guard regex.matches(in: month, options: [], range: NSMakeRange(0, month.characters.count)).count == 0 else {
            return false
        }
        guard regex.matches(in: year, options: [], range: NSMakeRange(0, year.characters.count)).count == 0 else {
            return false
        }

        let cal = NSCalendar.current
        let comps = DateComponents(year: Int(year)!, month: Int(month)!)
        let expDate = cal.date(from: comps)
        if let expDate = expDate {
            return expDate.timeIntervalSinceNow > 0
        } else {
            return false
        }
    }

    // Validate whether a credit card number passes the Luhn check
    static func validateLuhn(_ number: String) -> Bool {
        // 1. Reverse the characters
        // 2. Map the characters to ints
        // 3. Enumerate and reduce to a sum
        // 4. Perform the modulus check
        let sum = number.characters.reversed().map({Int(String($0))!}).enumerated().reduce(0) { (acc, elm) in
            let idx = elm.0
            var val = elm.1
            // Double and limit every odd index
            if idx % 2 != 0 {
                val *= 2
                // Values must be within the range of [0...9]
                if val > 0 {
                    val = val % 9
                }
            }
            return acc + val
        }
        return sum % 10 == 0
    }

}
