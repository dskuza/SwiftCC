# SwiftCC

## About

SwiftCC is an easy-to-use framework built with Swift 3 to validate credit cards.

## Usage

```swift
import SwiftCC
let cc = CreditCard(withString:"4111111111111111")
cc.securityCode = "111"
cc.expirationMonth = "12"
cc.expirationYear = "2020"
cc.issuer() // returns CreditCard.Issuer.Visa

// Validate card information
CreditCardValidator.validateNumber(cc.cardNumber, against: CreditCard.Issuer.Visa) // True, it's a Visa card
CreditCard.validateLuhn(cc.cardNumber) // True, it passes the Luhn check
CreditCardValidator.validateSecurityCode(cc.securityCode, against: cc.issuer()) // True, it's a 3 digit code

// Validate card expiry
CreditCardValidator.validateExpirationMonth(cc.expirationMonth) // True, it's between 1-12
CreditCardValidator.validateExpirationMonth(cc.expirationYear) // True, it's at least the current year
CreditCardValidator.validateExpirationYear(cc.expirationMonth) // True, it's after the current month/year
```
